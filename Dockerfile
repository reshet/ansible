FROM alpine:latest

MAINTAINER reshet

RUN apk update && \
  apk add py3-pip make gcc python3-dev libffi-dev libc-dev openssl-dev && \
  rm -rf /var/cache/apk/* && \
  pip install --upgrade pip && \
  export CRYPTOGRAPHY_DONT_BUILD_RUST=1 && \
  pip install ansible

USER root

WORKDIR /var/tmp
